package com.octest.beans;

import com.octest.exception.BeanException;

public class Utilisateur {
	private String nom;
	private String prenom;
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) throws BeanException {
		if(nom.length()>10) {
			throw new BeanException("le nom est trop grand! (10 caractères maximum)");
		}
		else {
			this.nom = nom;
		}
	}
	
	/*public void setNom(String nom) {
		this.nom = nom;
	}*/
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	
	

}
