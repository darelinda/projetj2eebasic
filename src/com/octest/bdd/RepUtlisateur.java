package com.octest.bdd;

import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import java.sql.Connection;
import java.sql.Statement;

import com.octest.beans.Utilisateur;
import com.octest.exception.BeanException;

public class RepUtlisateur {
	private Connection connexion = null;
	
	public List<Utilisateur> reupererUtilisateurs(){
		List<Utilisateur> utilisateurs = new ArrayList<Utilisateur>();
		Statement statement = null;
		ResultSet resultat = null;
		loadDatabase();
		try {
		//execution de la requ�te
		statement = connexion.createStatement();
		resultat = statement.executeQuery("SELECT nom, prenom FROM utilisateur;");
		// recuperation des donn�es
		while (resultat.next()) {
			String nom =resultat.getString("nom");
			String prenom =resultat.getString("prenom");
			Utilisateur utilisateur = new Utilisateur();
			try {
				utilisateur.setNom(nom);
			} catch (BeanException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			utilisateur.setPrenom(prenom);
			utilisateurs.add(utilisateur);
		}
		}
		catch (SQLException e) {
		}finally {
			//fermeture de connexion
			try {
				if(resultat!=null) {
					resultat.close();
				}
				if(statement!=null) {
					statement.close();
				}
				if(connexion!=null) {
					connexion.close();
				}
			}catch (SQLException ignore){}
		
	}
		return utilisateurs;
	}
private void loadDatabase() {
//Chargement du driver
	try {
		Class.forName("com.mysql.jdbc.Driver");
	} catch (ClassNotFoundException e) {
	}
	try {
		connexion= DriverManager.getConnection("jdbc:mysql://localhost:3306/testdarel", "root", "gaston");
	}
	catch (SQLException e) {
		e.printStackTrace();
	}			
}

public void ajouterUtilisateur(Utilisateur utilisateur) {
	loadDatabase();
	try {
		PreparedStatement preparedStatement = 
				connexion.prepareStatement("INSERT INTO utilisateur (nom,prenom) VALUES (?,?)");
		preparedStatement.setString(1, utilisateur.getNom());
		preparedStatement.setString(2, utilisateur.getPrenom());
		preparedStatement.executeUpdate();
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
		
	
}
}