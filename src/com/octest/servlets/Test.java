package com.octest.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.octest.beans.Utilisateur;
import com.octest.exception.BeanException;
import com.octest.exception.DaoException;
import com.octet.dao.DaoFactory;
import com.octet.dao.UtilisateurDao;

@WebServlet("/Test")
public class Test extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private UtilisateurDao utilisateurDao;
    
	public void init() throws ServletException{
		DaoFactory daoFactory =DaoFactory.getInstance();
		this.utilisateurDao = daoFactory.getUtilisateurDao();
	}
   
    public Test() {
        super();
        // TODO Auto-generated constructor stub
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/*String name =request.getParameter("name");
		request.setAttribute("name", name);
		Auteur auteur = new Auteur();
		auteur.setActif(true);
		auteur.setNom("Donfack");
		auteur.setPrenom("Darelle");
		request.setAttribute("auteur", auteur);
		String[] titres = {"nouvel l'incendie", "Lier pour la vie", "donner sa vie � Dieu"};
		request.setAttribute("titres", titres);
		Cookie[] cookies = request.getCookies();
		if(cookies!=null) {
			for(Cookie cookie :cookies) {
				if(cookie.getName().equals("prenom")) {
					request.setAttribute("prenomCookie", cookie.getValue());
				}
			}
		}*/
		
		/*RepUtlisateur tableUtilisateurs = new RepUtlisateur();
		List<Utilisateur> utilisateurs = new ArrayList<Utilisateur>();
		utilisateurs= tableUtilisateurs.reupererUtilisateurs()*/;
		try {
			request.setAttribute("utilisateurs",utilisateurDao.listUtilisateur());
		} catch (DaoException e) {
			request.setAttribute("erreur", e.getMessage());
		}
		//permet d'afficher la page jsp
		this.getServletContext().getRequestDispatcher("/WEB-INF/bonjour.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//ConnectionForm form = new ConnectionForm();
		//form.verifierIdentifiants(request);
		/*String nom = request.getParameter("nom");
		String prenom = request.getParameter("prenom");
		HttpSession session = request.getSession();
		session.setAttribute("nom", nom);
		session.setAttribute("prenom", prenom);
		Cookie cookie = new Cookie("prenom", prenom);
		cookie.setMaxAge(60*60*24*30);
		response.addCookie(cookie);*/
		Utilisateur user = new Utilisateur();
		try {
			user.setNom(request.getParameter("nom"));
		} catch (BeanException e) {
			request.setAttribute("erreur", e.getMessage()); 
		}
		user.setPrenom(request.getParameter("prenom"));
		try {
			utilisateurDao.ajouter(user);
		} catch (DaoException e) {
			request.setAttribute("erreur", e.getMessage());
		}
		try {
			request.setAttribute("utlisateurs",utilisateurDao.listUtilisateur());
		} catch (DaoException e) {
			request.setAttribute("erreur", e.getMessage());
		}
		
		doGet(request, response);
	}

}
