package com.octet.dao;

import java.util.List;

import com.octest.beans.Utilisateur;
import com.octest.exception.DaoException;

public interface UtilisateurDao {
	void ajouter (Utilisateur utilisateur) throws DaoException;
	List<Utilisateur> listUtilisateur() throws DaoException;
}
