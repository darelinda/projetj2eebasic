package com.octet.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.octest.beans.Utilisateur;
import com.octest.exception.BeanException;
import com.octest.exception.DaoException;

public class UtilisateurDaoImpl implements UtilisateurDao {
	
	private DaoFactory daoFactory;
	UtilisateurDaoImpl(DaoFactory daoFactory){
		this.daoFactory = daoFactory;
	}

	@Override
	public void ajouter(Utilisateur utilisateur) throws DaoException {
		PreparedStatement preparedStatement = null;
		Connection connexion = null;
		
		try {
			connexion = daoFactory.getConnection();
			preparedStatement = 
					connexion.prepareStatement("INSERT INTO utilisateur (nom, prenom) VALUES (?, ?)");
			preparedStatement.setString(1, utilisateur.getNom());
			preparedStatement.setString(2, utilisateur.getPrenom());
			preparedStatement.executeUpdate();
			connexion.commit();
		} catch (SQLException e) {
			try {
				if (connexion!=null) {
					connexion.rollback();
				}
			}catch(SQLException e2){
			}
			throw new DaoException ("Imposible de communiquer avec la base de donn�es");
			}finally {
				//fermeture de connexion
				try {
					
					if(connexion!=null) {
						connexion.close();
					}
				}catch (SQLException e){
					throw new DaoException ("Imposible de communiquer avec la base de donn�es");
				}
			}
	}

	@Override
	public List<Utilisateur> listUtilisateur() throws DaoException {
		List<Utilisateur> utilisateurs = new ArrayList<Utilisateur>();
		Statement statement = null;
		ResultSet resultat = null;
		Connection connexion = null;
		try {
			connexion = daoFactory.getConnection();
			//execution de la requ�te
			statement = connexion.createStatement();
			resultat = statement.executeQuery("SELECT nom, prenom FROM utilisateur;");
			// recuperation des donn�es
			while (resultat.next()) {
				String nom =resultat.getString("nom");
				String prenom =resultat.getString("prenom");
				Utilisateur utilisateur = new Utilisateur();
				try {
					utilisateur.setNom(nom);
				} catch (BeanException e) {
					e.printStackTrace();
				}
				utilisateur.setPrenom(prenom);
				utilisateurs.add(utilisateur);
			}
			
	} catch (SQLException e) {
		try {
			if (connexion!=null) {
				connexion.rollback();
			}
		}catch(SQLException e2){
		}
		throw new DaoException ("Imposible de communiquer avec la base de donn�es");
		}finally {
			//fermeture de connexion
			try {
				
				if(connexion!=null) {
					connexion.close();
				}
			}catch (SQLException e){
				throw new DaoException ("Imposible de communiquer avec la base de donn�es");
			}
		}


		return utilisateurs;
	}
	

}
